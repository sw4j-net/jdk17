# Introduction

This project creates a docker image with (open)jdk 17 installed.

The image can be used as a base image for other images or to run applications needing java.

This repository is mirrored to https://gitlab.com/sw4j-net/jdk17
